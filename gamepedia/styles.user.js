// ==UserScript==
// @name     Hide Gamepedia PRO-Editor banner
// @namespace userjs.sa-tas.net/gamepedia/styles
// @version  1
// @grant    none
// @match  https://*.gamepedia.com/*
// @run-at document-idle
// ==/UserScript==

(function () {
	var _siderail = document.getElementById('siderail_arksurvivalevolved_gamepedia');
	_siderail.parentElement.removeChild(_siderail);
	document.getElementById('bodyContent').style.width = 'auto';
})();
