// just to note for later ...
// with not so many channels subscribed,
// seems easiest way is to go to
//
// https://www.youtube.com/feed/subscriptions?flow=1
//
// and use following snippet to let it
// automatically load all pages with videos from subscribed channels
// and then use just browser built-in search
//
// for more complex stuff i had python using google data api
// but it seems outdated and i don't feel like fixing it now (when this is enough for me)


var mt = setInterval(function() { window.scrollTo(0, 200000); }, 2000);
// when loading visibly stopped, disable autoscrolling
clearInterval(mt)
