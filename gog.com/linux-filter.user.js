// ==UserScript==
// @name     GOG.com is missing filtering for Linux in promos and such
// @namespace userjs.sa-tas.net/gog.com/linux-filter
// @version  1
// @grant    none
// @match  https://www.gog.com/promo/*
// @run-at document-idle
// ==/UserScript==

(function(){
    if(prompt('QS Linux filter?', 'no') != 'yes') {
        return;
    }
    var items=document.getElementsByClassName('product-row');
    for(var idx=0; idx<items.length; idx++) {
        if(!items[idx].outerHTML.includes('icon-linux')) {
            items[idx].style.display='none';
        }
    }
})();
