// ==UserScript==
// @name     RQJenkins Unfold reldel views
// @namespace userjs.sa-tas.net/jenkins/qs-reldel-view
// @version  2
// @grant    none
// @match  https://rhos-ci-jenkins.lab.eng.tlv2.redhat.com/view/QE/view/*
// @run-at document-idle
// ==/UserScript==

setTimeout(function () {
    document.getElementById('jobNestedItems_Phase1').style.display = ''
    document.getElementById('jobNestedItems_Phase2').style.display = ''
    document.getElementById('jobNestedItems_Triggers').style.display = ''
}, 200);

