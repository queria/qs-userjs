// ==UserScript==
// @name     RQJenkins bigger textareas
// @namespace userjs.sa-tas.net/jenkins/textarea
// @version  4
// @grant    none
// @match  https://rhos-ci-jenkins.lab.eng.tlv2.redhat.com/script
// @require https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js
// @run-at document-idle
// ==/UserScript==

jQuery(document).ready(function() {
    var dynHeight = Math.max(200, window.innerHeight - 500);
    document.getElementById('script').style.height = dynHeight + 'px';
    document.getElementById('script').style.width = '1300px';
    jQuery('<style>').prop('type', 'text/css').html('.CodeMirror-scroll { height: '+dynHeight+'px !important; }').appendTo('head');
});
