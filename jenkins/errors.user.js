// ==UserScript==
// @name     RQJenkins Highlight errors in console
// @namespace userjs.sa-tas.net/jenkins/errors
// @version  8
// @grant    none
// @match  https://rhos-ci-jenkins.lab.eng.tlv2.redhat.com/*/console*
// @match  https://rhos-ci-staging-jenkins.lab.eng.tlv2.redhat.com/*/console*
// @match  http://staging-jenkins2-qe-playground.usersys.redhat.com/*/console*
// @require https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js
// @run-at document-idle
// ==/UserScript==


setTimeout(function() {
    var cons = jQuery('.console-output');
    if(!cons.length) { return; }
    if(cons[0].innerHTML.length > (10 * 1024 * 1024)) { return; /* dont process more than 10MB of output */ }
    var ers = jQuery('#qsErrors');
    if(!ers.length) {
        ers = jQuery('<ul id="qsErrors" style="position:fixed; right: 0px; width:10em; text-align:right; max-height:30em; overflow:auto; background: rgba(255,255,255,0.8);"></ul>')
        jQuery('#main-panel').after(ers);
        jQuery('<style>').prop('type', 'text/css').html('#qsErrors a { color: red; } #qsErrors a:hover { color: blue; } .qsError { color: #c00 !important; background:white; font-weight: bold; } .qsErrorLinked::before { content:" "; display:block; height:4em; }').appendTo('head');
    };
    var spanColorToErr = function(cons, color) {
        jQuery('span[style*="color:'+color+'"]', cons).addClass('qsError');
    }
    spanColorToErr(cons, 'rgb(255,85,85)');
    /* detect all presentations of errors here, space at beginning matters */
    spanColorToErr(cons, 'rgb(255,85,85)');
    spanColorToErr(cons, ' rgb(204, 0, 0)');
    spanColorToErr(cons, 'rgb(204,0,0)');
    spanColorToErr(cons, 'rgb(187,0,0)');
    spanColorToErr(cons, ' #c00');
    spanColorToErr(cons, ' #CD0000');

    var cnt=1;
    jQuery('.qsError').each(function() {
        /* group together errors with distance of 10 dom elements or less */
        var prevEl = jQuery(this).prev();
        var tmpChild = undefined;
        for(var prevCnt=1; prevCnt < 10; prevCnt++) {
            /* iterate over previous elements and if they or their child element are also errors
             * ignore this current one - do not add hyperlink for it */
            // console.log('got child: '); console.log(tmpChild);
            if(prevEl.hasClass('qsError')) { return; };
            tmpChild = prevEl.children()[0]
            if(tmpChild != undefined && jQuery(tmpChild).hasClass('qsError')) { return; };

            prevEl = prevEl.prev();
        };
        jQuery(this).attr('id', 'qsError'+cnt).addClass('qsErrorLinked');
        if(!jQuery('#qsErrorLink'+cnt).length) {
            ers.append(jQuery('<li id="qsErrorLink'+cnt+'"><a href="#qsError'+cnt+'">error #'+cnt+'</a></li>'));
        };
        cnt++;
    });
},
1000);
