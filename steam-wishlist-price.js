// JavaScript oneliner to calculate total price of your Steam Wishlist

// 1) visit page with your wishlist (http://store.steampowered.com/wishlist/id/YOUR-ID-HERE/)
//    (augment or remove any filters as you wish)
// 2) execute following script in dev-tools console (F12 - Console)
//
// note - it assumes price is one character at the end of price field (e.g. €)
//        if not modify splice(0, -1) accordingly.  // (0, -1) removes last character, (1) first, ...
//
alert("Total sum of wishlist items is: " + Math.round(jQuery.makeArray(jQuery('div.discount_final_price').map(function () {return Number(jQuery(this).text().slice(0, -1).replace(',', '.'));})).reduce((a, b) => a + b, 0)))
