#!/bin/bash

cd "$(dirname "$(readlink -f "$0")")/../.git/hooks"
ln -snf ../../.githooks/pre-commit ./pre-commit
