// ==UserScript==
// @name     Unfuck service-now's page title
// @namespace userjs.sa-tas.net/service-now/fixup
// @version  1
// @grant    none
// @match  https://*.service-now.com/*
// @require https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js
// @run-at document-idle
// ==/UserScript==

setTimeout(function () {
  document.title = $('div', $('.panel-body').eq(1)).eq(1).text().trim() + ' ~ ' + document.title;
}, 1500);

