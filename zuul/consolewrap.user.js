// ==UserScript==
// @name     Zuul Console text wrap
// @namespace userjs.sa-tas.net/zuul/consolewrap
// @version  2
// @grant    none
// @match  https://review.rdoproject.org/zuul/build/*
// @match  https://sf.hosted.upshift.rdu2.redhat.com/zuul/*/build/*
// @require https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js
// @run-at document-idle
// ==/UserScript==


setTimeout(function() {
    jQuery('<style id="qsStyle">').prop('type', 'text/css').html(
      'pre { white-space: pre-wrap; }'
    ).appendTo('head');

},
1000);

